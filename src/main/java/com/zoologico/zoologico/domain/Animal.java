package com.zoologico.zoologico.domain;


import com.zoologico.zoologico.domain.enumeration.GenderType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Animal {

    @Id
    private  String code;


    private String name;
    private String race;
    @Enumerated(EnumType.STRING)
    private GenderType gender;
    @ManyToOne
    private Location location;


}
