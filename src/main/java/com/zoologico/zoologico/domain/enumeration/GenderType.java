package com.zoologico.zoologico.domain.enumeration;

public enum GenderType {
    MALE, FEMALE
}
