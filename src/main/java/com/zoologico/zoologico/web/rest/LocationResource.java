package com.zoologico.zoologico.web.rest;

import com.zoologico.zoologico.domain.Location;
import com.zoologico.zoologico.service.ILocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/location")
public class LocationResource {

    @Autowired
    ILocationService locationService;

    @PostMapping("")
    public Location create(@RequestBody Location location){
        return locationService.create(location);
    }

    @GetMapping("")
    public Iterable<Location> read(){
        return locationService.read();
    }

}
