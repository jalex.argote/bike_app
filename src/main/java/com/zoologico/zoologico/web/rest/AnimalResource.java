package com.zoologico.zoologico.web.rest;

import com.zoologico.zoologico.domain.Animal;
import com.zoologico.zoologico.repository.dto.AnimalGender;
import com.zoologico.zoologico.repository.dto.AnimalNameGender;
import com.zoologico.zoologico.service.IAnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/animal")
public class AnimalResource {

    @Autowired
    IAnimalService animalService;

    @PostMapping("")
    public ResponseEntity create(@RequestBody Animal animal) {
        return animalService.create(animal);
    }

    //Get
    @GetMapping("")
    public Iterable<Animal> read() {
        return animalService.read();
    }


    @GetMapping("/count")
    public Integer countAnimals(){
        return animalService.quantityAnimals();
    }
    @GetMapping("/name-gender")
    public  Iterable<AnimalNameGender> getAnimalNameGender(){
        return animalService.getAnimalNameGender();
    }

    @GetMapping("/animal-male")
    public  Iterable<AnimalGender> getAnimalMale(){
        return animalService.getAnimalMale();
    }

    @GetMapping("/animal-female")
    public  Iterable<AnimalGender> getAnimalFemale(){
        return animalService.getAnimalFemale();
    }

    @GetMapping("/count-male")
    public  Integer countAnimalMale(){
        return animalService.quantityAnimalMale();
    }

    @GetMapping("/count-female")
    public  Integer countAnimalFemale(){
        return animalService.quantityAnimalFemale();
    }
}
