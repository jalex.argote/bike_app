package com.zoologico.zoologico.repository;

import com.zoologico.zoologico.domain.Animal;
import com.zoologico.zoologico.repository.dto.AnimalGender;
import com.zoologico.zoologico.repository.dto.AnimalNameGender;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


public interface AnimalRepository extends CrudRepository<Animal, String> {

    //Cantidad de animales que hay en el zoologico
    @Query(value = "select count (animal) from Animal animal")
    Integer quantityAnimals();

    //Lista los animales con su nombre y genero
    @Query(value = "select animal.name as name, animal.gender as gender from Animal animal")
    Iterable<AnimalNameGender> findAnimalNameGender();

    //Lista los animales por genero MALE
    @Query(value = "select animal.name as name from Animal animal where gender = 'MALE'")
    Iterable<AnimalGender> findAnimalMale();

    //lista los animales por genero FEMALE
    @Query(value = "select animal.name as name from Animal animal where gender = 'FEMALE'")
    Iterable<AnimalGender> findAnimalFemale();

    //Cuenta los animales de genero MALE
    @Query(value = "select count (animal) as male from Animal animal where gender = 'MALE'")
    Integer quantityAnimalMale();

    //Cuenta los animales de genero FEMALE
    @Query(value = "select count (animal) as female from Animal animal where gender = 'FEMALE'")
    Integer quantityAnimalFemale();

}
