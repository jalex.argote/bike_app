package com.zoologico.zoologico.repository;

import com.zoologico.zoologico.domain.Location;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface LocationRepository extends CrudRepository<Location, Integer> {

}
