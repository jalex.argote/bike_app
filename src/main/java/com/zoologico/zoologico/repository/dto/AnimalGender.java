package com.zoologico.zoologico.repository.dto;

public interface AnimalGender {
    public String getName();
}
