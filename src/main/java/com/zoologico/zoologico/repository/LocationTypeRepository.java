package com.zoologico.zoologico.repository;

import com.zoologico.zoologico.domain.LocationType;
import org.springframework.data.repository.CrudRepository;

public interface LocationTypeRepository extends CrudRepository<LocationType, Integer> {
}
