package com.zoologico.zoologico.service;

import com.zoologico.zoologico.domain.LocationType;
import org.springframework.http.ResponseEntity;

public interface ILocationTypeService  {
    public LocationType create(LocationType locationType);
    public Iterable<LocationType> read();
}
