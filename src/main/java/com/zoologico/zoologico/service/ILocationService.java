package com.zoologico.zoologico.service;

import com.zoologico.zoologico.domain.Location;
import org.springframework.http.ResponseEntity;

public interface ILocationService {

    public Location create(Location location);

    public Iterable<Location> read();

    //public Location update(Location location);
}
