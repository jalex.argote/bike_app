package com.zoologico.zoologico.service;

import com.zoologico.zoologico.domain.Animal;
import com.zoologico.zoologico.repository.dto.AnimalGender;
import com.zoologico.zoologico.repository.dto.AnimalNameGender;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface IAnimalService {

    public ResponseEntity create(Animal animal);

    public Iterable<Animal> read();

    public Animal update (Animal animal);

    public Optional<Animal> getById(String code);

    public Integer quantityAnimals();

    public Iterable<AnimalNameGender> getAnimalNameGender();

    public  Iterable<AnimalGender> getAnimalMale();

    public  Iterable<AnimalGender> getAnimalFemale();

    public  Integer quantityAnimalMale();

    public  Integer quantityAnimalFemale();

    //public Iterable<>
}
