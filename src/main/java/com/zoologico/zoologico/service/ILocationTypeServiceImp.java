package com.zoologico.zoologico.service;

import com.zoologico.zoologico.domain.LocationType;
import com.zoologico.zoologico.repository.LocationTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ILocationTypeServiceImp implements ILocationTypeService{

    @Autowired
    private LocationTypeRepository locationTypeRepository;


    @Override
    public LocationType create(LocationType locationType) {
        return locationTypeRepository.save(locationType);
    }

    @Override
    public Iterable<LocationType> read() {
        return locationTypeRepository.findAll();
    }
}
